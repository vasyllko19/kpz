﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;

namespace GyzVasyl.RobotChallange.Test
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void TestDistance()
        {
            var p1 = new Position(10, 2);
            var p2 = new Position(5, 8);
            var algorithm = new GyzVasylAlgorithm();
            Assert.AreEqual(algorithm.DistanceFind(p1, p2), 61);
        }

        [TestMethod]
        public void TestMoveCommand1()
        {
            var mymap = new Map();
            var position = new Position(10, 20);
            mymap.Stations.Add(new EnergyStation() { Energy = 1000, Position = position, RecoveryRate = 50 });
            var owner = new Owner() { Name = "Vasyl" };
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 100, OwnerName = owner.Name, Position = new Position(14, 10)}};
            var algorithm = new GyzVasylAlgorithm();
            var robotcommand = algorithm.DoStep(robots, 0, mymap);
            Assert.IsTrue(robotcommand is MoveCommand);
            Assert.AreEqual(((MoveCommand)robotcommand).NewPosition, new Position(13, 17));
        }

        [TestMethod]
        public void TestNearestSpot()
        {
            var map = new Map();
            var position = new Position(50, 40);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = position, RecoveryRate = 50 });
            var owner = new Owner() { Name = "Vasyl" };
            var rMyobots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 100, OwnerName = owner.Name, Position = new Position(1, 1)}};

            var algorithm = new GyzVasylAlgorithm();
            algorithm.MapLoad(map);
            var nearestPoint = algorithm.FindTheNearestPoint(rMyobots[0].Position, algorithm.cachedMap);
            Assert.AreEqual(nearestPoint, new Position(47, 37));
        }
        [TestMethod]
        public void FindFreePlace()
        {
            var point1 = new Position(25, 25);
            var point2 = new Position(24, 25);
            var Mystrategy = new GyzVasylAlgorithm();
            var fields = new Map();
            var listt = new List<Robot.Common.Robot>();

            listt.Add(new Robot.Common.Robot() { Energy = 120, Position = new Position(24, 24) });
            listt.Add(new Robot.Common.Robot() { Energy = 130, Position = new Position(30, 25) });


            Assert.AreEqual(Mystrategy.FindaFreePlace(point1, listt, fields), point2);

        }
        [TestMethod]
        public void TestMoveCommand2()
        {
            var mymap = new Map();
            var position = new Position(10, 20);
            mymap.Stations.Add(new EnergyStation() { Energy = 1000, Position = position, RecoveryRate = 50 });
            var owner = new Owner() { Name = "Vasyl" };
            var myrobots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 100, OwnerName = owner.Name, Position = new Position(14, 10)}};
            var algorithm = new GyzVasylAlgorithm();
            var Mycommand = algorithm.DoStep(myrobots, 0, mymap);
            Assert.IsTrue(Mycommand is MoveCommand);
            Assert.AreEqual(((MoveCommand)Mycommand).NewPosition, new Position(13, 17));
        }

        [TestMethod]
        public void TestCollect()
        {
            var mymap = new Map();
            var position = new Position(10, 20);
            mymap.Stations.Add(new EnergyStation() { Energy = 1000, Position = position, RecoveryRate = 50 });
            var owner = new Owner() { Name = "Vasyl" };
            var myrobots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 100, OwnerName = owner.Name, Position = new Position(9, 23)}};
            var algorithm = new GyzVasylAlgorithm();
            var command = algorithm.DoStep(myrobots, 0, mymap);
            Assert.IsTrue(command is CollectEnergyCommand);
        }
        [TestMethod]
        public void TestMoveCommand3()
        {
            var mymap = new Map();
            var position = new Position(23, 23);
            mymap.Stations.Add(new EnergyStation() { Energy = 500, Position = position, RecoveryRate = 30 });
            var owner = new Owner() { Name = "Vasyl" };
            var myrobots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 100, OwnerName = owner.Name, Position = new Position(14, 10)}};
            var algorithm = new GyzVasylAlgorithm();
            var doStep = algorithm.DoStep(myrobots, 0, mymap);
            Assert.IsTrue(doStep is MoveCommand);
            Assert.AreEqual(((MoveCommand)doStep).NewPosition, new Position(17, 15));
        }
        [TestMethod]

        public void IsValidPosition()
        {
            var post_point_1 = new Position(25, 25);
            var strategy = new GyzVasylAlgorithm();
            Assert.IsTrue(strategy.IsNormalValid(post_point_1));
        }

        [TestMethod]
        public void TestRobotMovement4()
        {
            var field = new Map();
            var point_1 = new Position(25, 25);
            var point_2 = new Position(50, 25);
            field.Stations.Add(new EnergyStation() { Energy = 320, Position = point_1, RecoveryRate = 40 });
            field.Stations.Add(new EnergyStation() { Energy = 320, Position = point_2, RecoveryRate = 40 });
            var gamer = new Owner() { Name = "Vasyl" };
            var myrobots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 120, OwnerName = gamer.Name, Position = new Position(24, 24)},
                new Robot.Common.Robot() {Energy = 130, OwnerName = gamer.Name, Position = new Position(30, 25)}
            };
            var strategy = new GyzVasylAlgorithm();
            var robot_movement_function = strategy.DoStep(myrobots, 1, field);
            Assert.IsTrue(robot_movement_function is MoveCommand);
            Assert.AreEqual(((MoveCommand)robot_movement_function).NewPosition, new Position(28, 25));
        }

        [TestMethod]

        public void GoToDestination()
        {
            var post_point_1 = new Position(25, 25);
            var post_point_2 = new Position(50, 25);
            int energy = 100;
            var strategy = new GyzVasylAlgorithm();
            Assert.IsFalse(strategy.CanGoToHheDestination(post_point_1, post_point_2, energy, 5));
        }




    }
}
