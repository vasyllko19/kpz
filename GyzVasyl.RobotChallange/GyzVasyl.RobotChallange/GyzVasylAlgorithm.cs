﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GyzVasyl.RobotChallange
{

    public class GyzVasylAlgorithm : IRobotAlgorithm
    {
        public Dictionary<Position, int> cachedMap { get; }
        public Dictionary<Position, int> cachedMap2 { get; }
        private readonly int dist = 3;
        private bool isitIsUpdated = false;
        public string Author => "Gyz Vasyl";
        private readonly int dimension = 100;

        public int MyRound = 0;

        public RobotCommand DoStep(IList<Robot.Common.Robot> Myrobots, int ToMoveIndex, Map Mymap)
        {
            if (!isitIsUpdated) MapLoad(Mymap);
            var nerestRobots = Myrobots[ToMoveIndex];
            var MTemp = ModernizeMap(Mymap, Myrobots, Myrobots.Count, ToMoveIndex).ToDictionary(entry => entry.Key.Copy(), entry => entry.Value);
            var freePlace = FindaFreePlace(nerestRobots.Position, Myrobots, Mymap);
            var myNewPlace = FindTheNearestPoint(freePlace, MTemp);
            var CountOfMyRobots = Myrobots.Where(robot => robot.OwnerName.Equals(nerestRobots.OwnerName)).ToList().Count;

            if (myNewPlace != null && nerestRobots.Energy >= 250 && CountOfMyRobots < 100 && CanGoToHheDestination(freePlace, myNewPlace, 200, 45))
            {
                cachedMap2[myNewPlace] = Myrobots.Count;
                return new CreateNewRobotCommand() { NewRobotEnergy = 200 };
            }

            RerengeTheStationMark(ToMoveIndex);
            MTemp = ModernizeMap(Mymap, Myrobots, ToMoveIndex, ToMoveIndex).ToDictionary(entry => entry.Key.Copy(), entry => entry.Value);
            myNewPlace = FindTheNearestPoint(nerestRobots.Position, MTemp);

            if (nerestRobots.Position.Equals(myNewPlace))
            {
                cachedMap2[nerestRobots.Position] = ToMoveIndex;
                return new CollectEnergyCommand();
            }

            if (myNewPlace != null && CanGoToHheDestination(nerestRobots.Position, myNewPlace, nerestRobots.Energy, 46))
            {
                cachedMap2[myNewPlace] = ToMoveIndex;
                if (DistanceFind(nerestRobots.Position, myNewPlace) <= nerestRobots.Energy)
                    return new MoveCommand() { NewPosition = myNewPlace };
                else
                {
                    myNewPlace = FindTheShortestandFastestWay(Myrobots, ToMoveIndex, myNewPlace);
                    if (cachedMap.ContainsKey(myNewPlace))
                        return new MoveCommand() { NewPosition = myNewPlace };
                }
            }
            return new CollectEnergyCommand();
        }


        public GyzVasylAlgorithm()
        {
            cachedMap = new Dictionary<Position, int>();
            cachedMap2 = new Dictionary<Position, int>();
            for (int i = 0; i < dimension; i++)
                for (int j = 0; j < dimension; j++)
                {
                    cachedMap.Add(new Position(i, j), 0);
                    cachedMap2.Add(new Position(i, j), -1);
                }
            Logger.OnLogRound += ChangeRound;
        }


        public bool CanGoToHheDestination(Position start, Position end, int energy, int tempRound)//функція яка перевіряє чи можна дрступитись до даної точки
        {
            var ddx = end.X - start.X;
            var ddy = end.Y - start.Y;
            var dx = Math.Abs(end.X - start.X);
            var dy = Math.Abs(end.Y - start.Y);
            var canreach = tempRound - MyRound - (dx > dy ? dx : dy) >= 0 && (dx + dy <= Math.Min(energy, 300));
            return canreach;
        }


        private void ChangeRound(object sender, LogRoundEventArgs e)//функція яка змінює райунд
        {
            MyRound++;
        }

        public int DistanceFind(Position a, Position b)//функція яка шукає дистанцію
        {
            var distance = (a.X - b.X) * (a.X - b.X) * +(a.Y - b.Y) * (a.Y - b.Y);

            return  (int)distance;
        }

        public bool IsNormalValid(Position position)//фунціz яка перевіряє чи робот який заспавнився є дійсний
        {
            var isValid = (position.X >= 0) && (position.X < 100) && (position.Y >= 0) &&
                          (position.Y < 100);
            return isValid;
        }



        public void MapLoad(Map Mymap)//функція яка загружає карту
        {
            for (int i = 0; i < Mymap.Stations.Count; i++)
            {
                List<Position> ListPositions = new List<Position>();
                for (int j = Mymap.Stations[i].Position.X - dist; j <= Mymap.Stations[i].Position.X + dist; j++)
                    for (int q = Mymap.Stations[i].Position.Y - dist; q <= Mymap.Stations[i].Position.Y + dist; q++)
                        ListPositions.Add(new Position(j, q));
                foreach (var possition in ListPositions)
                    if (cachedMap.ContainsKey(possition))
                        cachedMap[possition] += 1;
            }

            isitIsUpdated = true;
        }

        private Dictionary<Position, int> ModernizeMap(Map map, IList<Robot.Common.Robot> robots, int Indexx, int ToMoveIndex) //функція яка оновлює карту
        {
            var newMymap = cachedMap.ToDictionary(entry => entry.Key.Copy(), entry => entry.Value);
            List<EnergyStation> StationUSed = new List<EnergyStation>();
            for (int i = 0; i < Indexx; i++)
                if (!robots[i].OwnerName.Equals(robots[ToMoveIndex].OwnerName))
                {

                    StationUSed.AddRange(map.Stations.Where(station => (Math.Abs(station.Position.X - robots[i].Position.X) <= dist)
                        && (Math.Abs(station.Position.Y - robots[i].Position.Y) <= dist)).ToList());
                    StationUSed = StationUSed.Distinct().ToList();
                }

            List<Position> tempPossition = cachedMap2.Where(pair => pair.Value >= 0).Select(pos => pos.Key).ToList();
            foreach (var poss in tempPossition)
            {
                StationUSed.AddRange(map.Stations.Where(station => (Math.Abs(station.Position.X - poss.X) <= dist)
                    && (Math.Abs(station.Position.Y - poss.Y) <= dist)).ToList());
                StationUSed = StationUSed.Distinct().ToList();
            }
            foreach (var station in StationUSed)
            {
                List<Position> positions = new List<Position>();
                for (int j = station.Position.X - dist; j <= station.Position.X + dist; j++)
                    for (int k = station.Position.Y - dist; k <= station.Position.Y + dist; k++)
                        positions.Add(new Position(j, k));
                foreach (var pos in positions)
                    if (newMymap.ContainsKey(pos))
                        newMymap[pos] -= 1;
            }
            return newMymap;
        }

        public Position FindaFreePlace(Position point, object RobotList, Map Fields)
        {
            throw new NotImplementedException();
        }

        public Position FindTheNearestPoint(Position CurrPossition, Dictionary<Position, int> MapTemp)//функція яка шукає найближче місце
        {
            Position Near = null;
            int Countt = 0;

            for (int i = 10; i >= 1; i--)
            {
                Position tempNearestPos = null;
                var positions = MapTemp.Where(pair => pair.Value == i && cachedMap2[pair.Key] == -1).Select(pos => pos.Key).ToList();
                if (positions.Count > 0)
                {
                    tempNearestPos = positions[0];
                    int minimalDistance = DistanceFind(CurrPossition, positions[0]);
                    for (int j = 1; j < positions.Count; j++)
                        if (DistanceFind(CurrPossition, positions[j]) < minimalDistance)
                        {
                            tempNearestPos = positions[j];
                            minimalDistance = DistanceFind(CurrPossition, positions[j]);
                        }
                    if (Near == null)
                    {
                        Near = tempNearestPos;
                        Countt = i;
                    }
                    else if (minimalDistance * Countt < DistanceFind(CurrPossition, Near) * i)
                    {
                        Near = tempNearestPos;
                        Countt = i;
                    }
                }
            }
            return Near;
        }

        private void RerengeTheStationMark(int ToMoveIndex)//функція яка знімає позначку зі станції
        {
            var tempPositions = cachedMap2.Where(pair => pair.Value == ToMoveIndex).Select(pos => pos.Key).ToList();
            foreach (var pos in tempPositions)
                cachedMap2[pos] = -1;
        }

        private Position FindTheShortestandFastestWay(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Position end)// функція яка шукає найшвидший шлях
        {
            Position start = robots[robotToMoveIndex].Position.Copy();
            var ddx = end.X - start.X;
            var ddy = end.Y - start.Y;
            var dx = Math.Abs(end.X - start.X);
            var dy = Math.Abs(end.Y - start.Y);
            int k;
            if (robots[robotToMoveIndex].Energy < 300)
            {
                k = robots[robotToMoveIndex].Energy;
            }
            else
            {
                k = 300;
            }

            var energy = Math.Min(robots[robotToMoveIndex].Energy, 300);

            for (int i = 2; true; i++)
            {
                int DXtemp, DYtemp, EnergyTemp;
                if (dx < dy)
                {
                    DXtemp = (dx / i == 0 && dx > 0 ? 1 : dx / i);
                    DYtemp = (int)Math.Ceiling(1.0 * dy / i);
                }
                else
                {
                    DYtemp = (dy / i == 0 && dy > 0 ? 1 : dy / i);
                    DXtemp = (int)Math.Ceiling(1.0 * dx / i);
                }
                EnergyTemp = (int)Math.Ceiling(1.0 * energy / i);

                if (DXtemp * DXtemp + DYtemp * DYtemp <= EnergyTemp)
                {
                    int p;
                    if (end.X - start.X < 0)
                    {
                        p = -1;
                    }
                    else
                    {
                        p = 1;
                    }
                    int t;
                    if (end.Y - start.Y < 0)
                    {
                        t = -1;
                    }
                    else
                    {
                        t = 1;
                    }

                    start.X = start.X + DXtemp * Math.Sign(end.X - start.X);
                    start.Y = start.Y + DYtemp * Math.Sign(end.Y - start.Y);
                    bool checkk = true;
                    foreach (var robot in robots)
                        if (robot.Position.Equals(start))
                        {
                            checkk = false;
                            break;
                        }

                    if (!checkk)
                    {
                        int m;
                        if (end.X - start.X < 0)
                        {
                            m = -1;
                        }
                        else
                        {
                            m = 1;
                        }

                        int n;
                        if (end.Y - start.Y < 0)
                        {
                            n = -1;
                        }
                        else
                        {
                            n = 1;
                        }
                        if (dx > dy)
                            start.X = start.X + (DXtemp - 1) * Math.Sign(end.X - start.X);
                        else
                            start.Y = start.X + (DYtemp - 1) * Math.Sign(end.Y - start.Y);
                    }

                    return start;
                }
            }
        }

        public Position FindaFreePlace(Position nearestPosition, IList<Robot.Common.Robot> MyRobots, Map Mymap)//функція яка шукає вільну комірку
        {
            var Lenght = 1;
            while (Lenght < 100)
            {
                for (int i = -Lenght; i <= Lenght; i++)
                    for (int j = -Lenght; j <= Lenght; j++)
                    {
                        var newPos = new Position(nearestPosition.X + i, nearestPosition.Y + j);
                        if (!IsNormalValid(newPos))
                            continue;

                        if (!MyRobots.Any(r => r.Position == newPos))
                            return newPos;
                    }
                Lenght++;
            }
            throw new Exception("All cells are filled");
        }
        

    }
}

